package dominio;

import java.time.*;

public class Profesional extends Empleado {
	private static Double adicionalXTitulo = 20000.00;
	private Integer cantidadTitulos;
	
	
	public Integer getCantidadTitulos() {
		return cantidadTitulos;
	}
	public void setCantidadTitulos(Integer cantidadTitulos) {
		this.cantidadTitulos = cantidadTitulos;
	}


	public Profesional() {
		super();
		this.cantidadTitulos=0;
	}
	

	public Profesional(Integer legajo, Integer dni, String nombre, LocalDate fechaIngreso,
			Integer cantidadHijos, Integer cantidadTitulos) {
		super(legajo, dni, nombre, fechaIngreso, cantidadHijos);
		// TODO Auto-generated constructor stub
		this.cantidadTitulos = cantidadTitulos;
	}
	
	@Override
	public Double getRemunerativoBonif() {
		return super.getRemunerativoBonif()+this.getCantidadTitulos()*adicionalXTitulo;
	}
	

}
