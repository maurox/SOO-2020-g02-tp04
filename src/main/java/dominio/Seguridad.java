package dominio;

import java.time.*;

public class Seguridad extends Empleado {
	private static Double adicXtareaRiesgosa = 30000.00;
	private Integer cantTareasRiesgosas;
	
	
	public Integer getCantTareasRiesgosas() {
		return cantTareasRiesgosas;
	}
	public void setCantTareasRiesgosas(Integer cantTareasRiesgosas) {
		this.cantTareasRiesgosas = cantTareasRiesgosas;
	}

	public Seguridad(){
		super();
		this.cantTareasRiesgosas=0;
	}
	
	public Seguridad(Integer legajo, Integer dni, String nombre, LocalDate fechaIngreso,
			Integer cantidadHijos, Integer cantTareasRiesgosas) {
		super(legajo, dni, nombre, fechaIngreso, cantidadHijos);
		// TODO Auto-generated constructor stub
		this.cantTareasRiesgosas = cantTareasRiesgosas;
	}
	
	@Override
	public Double getRemunerativoBonif() {
			return super.getRemunerativoBonif()+this.getCantTareasRiesgosas()*adicXtareaRiesgosa;
	}
	

}
