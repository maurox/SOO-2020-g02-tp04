package dominio;

import java.time.*;

public class Administrativo extends Empleado {
	private Categoria categoria;
	
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}


	public Administrativo() {
		super();
		categoria=Categoria.SINCATEGORIA;
	}
	

	public Administrativo(Integer legajo, Integer dni, String nombre, LocalDate fechaIngreso,
			Integer cantidadHijos, Categoria categoria) {
		super(legajo, dni, nombre, fechaIngreso, cantidadHijos);
		// TODO Auto-generated constructor stub
		this.categoria = categoria;
	}
	
	@Override
	public Double getRemunerativoBonif() {
		return super.getRemunerativoBonif()+this.categoria.getAdicional();
	}
	
}
