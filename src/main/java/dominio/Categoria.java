package dominio;

public enum Categoria {
	SINCATEGORIA,
	CATEGORIA1,
	CATEGORIA2,
	CATEGORIA3,
	CATEGORIA4,
	CATEGORIA5;
	
	public Double getAdicional() {
		switch (super.toString()) {
		case "SINCATEGORIA": return 0.0;
		case "CATEGORIA1": return 5000.0;
		case "CATEGORIA2": return 7000.0;
		case "CATEGORIA3": return 9000.0;
		case "CATEGORIA4": return 15000.0;
		case "CATEGORIA5": return 20000.0;
		default: return 0.00;
		}
	}

}
