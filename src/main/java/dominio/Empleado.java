package dominio;


import java.time.*;

public abstract class Empleado {
	private static Double salarioBasico = 40000.00;
	private static Double adicXAntiguedad = 2000.00;
	private static Double adicXHijo = 3000.00;
	private static Double porcDescuento = 0.15;
	private static Integer nextID=100;
	
	
	private Integer id;
	private Integer legajo;
	private Integer dni;
	private String nombre;
	private LocalDate fechaIngreso;
	private Integer cantidadHijos;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getLegajo() {
		return legajo;
	}
	public void setLegajo(Integer legajo) {
		this.legajo = legajo;
	}
	public Integer getDni() {
		return dni;
	}
	public void setDni(Integer dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public Integer getCantidadHijos() {
		return cantidadHijos;
	}
	public void setCantidadHijos(Integer cantidadHijos) {
		this.cantidadHijos = cantidadHijos;
	}
	public Integer getAtiguedad() {
		return LocalDate.now().getYear()-this.fechaIngreso.getYear();
	}
	

	
	public Empleado() {};
	
	public Empleado(Integer legajo, Integer dni, String nombre, LocalDate fechaIngreso,
			Integer cantidadHijos) {
		nextID++;
		this.id = nextID;
		this.legajo = legajo;
		this.dni = dni;
		this.nombre = nombre;
		this.fechaIngreso = fechaIngreso;
		this.cantidadHijos = cantidadHijos;
	}
	
	/**
	 * Devuelve el remunerativo bonificable
	 * formula: Salario Basico + Antiguedad*AdicXantiguedad
	 * @return
	 */
	public Double getRemunerativoBonif() {
		return salarioBasico + adicXAntiguedad*getAtiguedad();
	}
	
	/**
	 * Devuelve el salario famiiliar
	 * formula: Cantidad Hijos * AdicXhijo
	 * @return
	 */
	public Double getSalarioFamiliar() {
		return adicXHijo*getCantidadHijos();
	}
	
	/**
	 * Devuelve el descuento aplicado al empleado
	 * formula: Remunerativo Bonificable * %Descuento
	 * @return
	 */
	public Double getDescuento() {
		return getRemunerativoBonif()*porcDescuento;
	}
	
	/**
	 * Devuelve el sueldo neto del empleado
	 * formula: remunerativo bonificable + salario familiar - descuento
	 * formula2: Remunerativo Bonificable * (1-%Descuento) + Salario Familiar
	 * @return
	 */
	public Double getSueldoNeto() {
		//return getRemunerativoBonif()+getSalarioFamiliar()-getDescuento();
		return getRemunerativoBonif()*(1-porcDescuento) + getSalarioFamiliar();
	}
	
	@Override
	public String toString() {
		return "Empleado [legajo=" + legajo + ", dni=" + dni + ", nombre=" + nombre + ", fechaIngreso=" + fechaIngreso
				+ ", cantidadHijos=" + cantidadHijos + ", atiguedad=" + getAtiguedad() + "]";
	}
	
}
