package manager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import dominio.Empleado;

public class ManagerEmpleado {
	private List<Empleado> listaEmpleados;
	
	public ManagerEmpleado() {
		listaEmpleados = new ArrayList<>();
	}
	
	public Integer contarEmpleado() {
		return listaEmpleados.size();
	}
	
	public Empleado buscarEmpleadoDNI(Integer dni) {
		
		for(Empleado e: listaEmpleados) {
			if(e.getDni().equals(dni)) {
				return e;
			}
		}
		return null;
	}
	
	public Empleado buscarEmpleadoLegajo(Integer legajo) {
		
		for(Empleado e: listaEmpleados) {
			if(e.getLegajo().equals(legajo)) {
				return e;
			}
		}
		return null;
	}
	
	public void agregarEmpleado(Empleado empleado) {
		listaEmpleados.add(empleado);
	}
	
	public void eliminarEmpleado(Empleado empleado) {
		listaEmpleados.remove(empleado);
	}
	
	public void listarEmpleados() {
		for (Empleado e:listaEmpleados) {
			System.out.println(e);
		}
	}
	
	public Integer contarEmpleadosXAntiguedad(Integer limite) {
		int contador = 0;
		double acumulador = 0.0;
		for (Empleado e:listaEmpleados) {
			if( e.getAtiguedad()>limite) {
				contador ++;
				acumulador += e.getSueldoNeto();
			}
		}
		System.out.println("Total acumulado: $"+ acumulador);
		return contador;
	}
	
	public Double sumarSaldosXAntiguedad(Integer limite) {
		double acumulador = 0.0;
		for (Empleado e:listaEmpleados) {
			if( e.getAtiguedad()>limite) {
				acumulador += e.getSueldoNeto();
			}
		}
		return acumulador;
	}
	
	

	public Empleado buscarXMayorCantHijos() {
		List<Empleado> listaAux = listaEmpleados;
		Comparator<Empleado> compareByCantidadHijos = (Empleado e1, Empleado e2) -> e1.getCantidadHijos().compareTo(e2.getCantidadHijos());
		listaAux.sort(compareByCantidadHijos.reversed());
		return listaAux.get(0);
	}
	
	public Integer contarEmpleadosXAnio(Integer anio) {
		int contador=0;
		for(Empleado e: listaEmpleados) {
			if(Integer.valueOf(e.getFechaIngreso().getYear()).equals(anio)) {
				contador++;
			}
		}
		return contador;
	}

}
