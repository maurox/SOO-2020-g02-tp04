package tp3poo.GestionEmpleados;

import static org.junit.Assert.*;
import java.time.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dominio.Administrativo;
import dominio.Categoria;
import manager.ManagerEmpleado;
import dominio.Profesional;
import dominio.Seguridad;

public class TestCaseEmpresa {
	
	ManagerEmpleado target;
	Profesional profesional1;
	Profesional profesional2;
	Administrativo administrativo1;
	Administrativo administrativo2;
	Seguridad seguridad1;	
	Seguridad seguridad2;

	@Before
	public void setUp() throws Exception {
		target = new ManagerEmpleado();
		System.out.println("Iniciando la prueba...");
		profesional1 = new Profesional(1001,10001,"Julian Correa",LocalDate.now().minusYears(10),2,2);
		profesional2 = new Profesional(1002,10002,"Juan Gabriel",LocalDate.now().minusYears(4),4,1);
		administrativo1 = new Administrativo(1003,10003,"Lorena Fuentes",LocalDate.now().minusYears(7),4,Categoria.CATEGORIA4);
		administrativo2 = new Administrativo(1004,10004,"Matias Perez",LocalDate.now().minusYears(3),3,Categoria.CATEGORIA3);
		seguridad1 = new Seguridad(1005,10005,"Fernando Flores",LocalDate.now().minusYears(5),1,1);
		seguridad2 = new Seguridad(1006,10006,"Micaela Lopez",LocalDate.now().minusYears(4),0,0);
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Fin de la prueba...");
	}

	
	@Test
	public void testAgregar() {
		System.out.println("Probando agregar...");

		target.agregarEmpleado(profesional1);
		target.agregarEmpleado(profesional2);
		target.agregarEmpleado(administrativo1);
		target.agregarEmpleado(administrativo2);
		target.agregarEmpleado(seguridad1);
		target.agregarEmpleado(seguridad2);
		int totalEmpleados = target.contarEmpleado();
		assertEquals(6, totalEmpleados);
	}
	
	@Test
	public void testEliminar() {
		System.out.println("Probando elimnar...");

		target.agregarEmpleado(profesional1);
		target.agregarEmpleado(profesional2);
		target.agregarEmpleado(administrativo1);
		target.agregarEmpleado(administrativo2);
		target.agregarEmpleado(seguridad1);
		target.agregarEmpleado(seguridad2);
		target.eliminarEmpleado(seguridad1);
		int totalEmpleados = target.contarEmpleado();
		assertEquals(5, totalEmpleados);
	}	
	
	
	@Test
	public void testPorAntiguedad() {
		System.out.println("Probando Contar Empleados y Sumar Sueldos por Antiguedad...");
		target.agregarEmpleado(profesional1);
		target.agregarEmpleado(profesional2);
		target.agregarEmpleado(administrativo1);
		target.agregarEmpleado(administrativo2);
		target.agregarEmpleado(seguridad1);
		target.agregarEmpleado(seguridad2);
		int totalEmpleados = target.contarEmpleadosXAntiguedad(4);
		double totalSaldosEmpleados = target.sumarSaldosXAntiguedad(4);
		assertEquals(3, totalEmpleados);
		assertEquals(232650.0, totalSaldosEmpleados, 0.0001);
	}
	
	@Test
	public void testBuscarPorMasHijos() {
		System.out.println("Probando buscar Empleado con mas hijos...");
		target.agregarEmpleado(profesional1);
		target.agregarEmpleado(profesional2);
		target.agregarEmpleado(administrativo1);
		target.agregarEmpleado(administrativo2);
		target.agregarEmpleado(seguridad1);
		target.agregarEmpleado(seguridad2);
		Object empleadoConMasHijos = target.buscarXMayorCantHijos();
		assertNotNull(empleadoConMasHijos);
		
	}
	
	@Test
	public void testContarPorAnioIngreso() {
		System.out.println("Probando contar Empleados por año de ingreso...");
		target.agregarEmpleado(profesional1);
		target.agregarEmpleado(profesional2);
		target.agregarEmpleado(administrativo1);
		target.agregarEmpleado(administrativo2);
		target.agregarEmpleado(seguridad1);
		target.agregarEmpleado(seguridad2);
		int empleadosAnioX = target.contarEmpleadosXAnio(2017);
		assertEquals(1, empleadosAnioX);
		
	}
	
	@Test
	public void testCalculoSueldo() {
		System.out.println("Probando calculo de sueldo");
		target.agregarEmpleado(profesional2);
		 double sueldo = profesional2.getSueldoNeto();
		 assertEquals(69800.00, sueldo, 0.0001);	
		}	
	}
	

